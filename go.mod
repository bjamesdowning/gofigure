module bitbucket.org/bjamesdowning/gofigure

go 1.13

require (
	github.com/go-redis/redis v6.15.6+incompatible
	github.com/satori/go.uuid v1.2.0
	golang.org/x/crypto v0.0.0-20200128174031-69ecbb4d6d5d
)
